This is the latest build of DrawSomethingSpy.

It works by searching for the PID of Draw Something, dumping its memory to a file with dd, and trying to parse the dump in search of the word to be found.

 It didn't work with later versions of Draw Something, and I couldn't get it to work anymore, so I pulled it from the store.
 
 Now I'm releasing it to the public, hoping somebody has any use for it.
 
 If you think this code was helpful, please mention my name and send me an email/tweet/anything to let me know you did.
 
 Thanks for reading this, have a nice day!
 
 Ted van den Brink
 ted@digistate.nl
 @tedvdb
 http://peejseej.nl/ 