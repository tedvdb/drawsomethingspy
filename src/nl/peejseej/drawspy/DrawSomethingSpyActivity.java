/*This file is part of DrawSomethingSpy.
 * 
 * DrawSomethingSpy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * DrawSomethingSpy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with DrawSomethingSpy.  If not, see <http://www.gnu.org/licenses/>.
 */

package nl.peejseej.drawspy;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class DrawSomethingSpyActivity extends Activity {

	private static String DUMPPATH = Environment.getExternalStorageDirectory().getPath()+"/.tmpdump";
	private static String STRINGDUMPPATH = Environment.getExternalStorageDirectory().getPath()+"/.stringdump";
	private static String MAPSPATH = Environment.getExternalStorageDirectory().getPath()+"/.drawmaps";

	private static int DSTFREE = 0;
	private static int DSTPAID = 1;

	private static int VARIANT = 0;
	private static int PID = 1;	

	private final int totalsteps = 0;

	TextView t;
	TextView found;
	ImageView iv;
	ImageButton imageButton;
	ProgressBar pb;
	ProgressBar stepBar;


	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		t = (TextView)findViewById(R.id.txtResult);
		found = (TextView)findViewById(R.id.foundWord);

		iv = (ImageView)findViewById(R.id.imageButton1);
		pb = (ProgressBar)findViewById(R.id.progressBar1);
		stepBar = (ProgressBar)findViewById(R.id.stepBar);
		stepBar.setMax(totalsteps);
		startSearch();

		imageButton = (ImageButton) findViewById(R.id.imageButton1);
		imageButton.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				startSearch();
			}
		});

	}   

	private void startSearch()
	{
		iv.setVisibility(View.INVISIBLE);
		pb.setVisibility(View.VISIBLE);

		found.setTextColor(Color.WHITE);
		found.setBackgroundColor(Color.BLACK);


		found.setText("Searching...");
		t.setText("Search started...\n");

		AsyncDataCollector task = new AsyncDataCollector();
		task.execute();
	}

	public void notRoot()
	{
		Log.d("ERROR","No root!");
		showErrorAndDie("This application requires root access. Exiting.");
	}

	public void showErrorAndDie(String s)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(s)
		.setCancelable(false)
		.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				DrawSomethingSpyActivity.this.finish();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	} 

	@SuppressLint("SimpleDateFormat")
	private class AsyncDataCollector extends AsyncTask<Void, String, String> {

		@Override
		protected void onPostExecute(String result) {
			TextView found = (TextView)findViewById(R.id.foundWord);
			if(result!=null && !result.equals("")) {
				found.setBackgroundColor(Color.GREEN);
				found.setTextColor(Color.BLACK);
				found.setText(" "+result+" ");
			} else {
				found.setBackgroundColor(Color.RED);
				found.setTextColor(Color.WHITE);
				found.setText("Not found.");
			}
			pb.setVisibility(View.INVISIBLE);
			iv.setVisibility(View.VISIBLE);

		}

		protected void onProgressUpdate(String... update)
		{
			t.append(update[0]);		
		}


		@Override
		protected String doInBackground(Void... params) {
			//publishProgress(currentstep++);

			String foundword = null;

			try {  
				int pid[] = findPid();
				///publishProgress(currentstep++);
				if(pid[PID]==-1 || pid[PID]==0) {
					publishProgress("Please make sure Draw Something is running.\nPress the image top-right to retry.\n");
				} else {
					if (pid[VARIANT]==DSTFREE)
						publishProgress("Found PID of Draw Something Free: "+pid[PID]+"\n");
					if (pid[VARIANT]==DSTPAID)
						publishProgress("Found PID of Draw Something Free: "+pid[PID]+"\n");

					//write memory maps to file
					File f = dumpMaps(pid);
					if(f==null) {
						publishProgress("Failed to dump memory. Strange...\nI don't know what to do now. :(\n");
						return "";
					}	            	

					foundword = readMemoryDump(pid);      	
					//if(foundword==null)
				}


				//System.out.println("Done");
			} catch (IOException e) {  
				e.printStackTrace();
				publishProgress("Error: " + e.getMessage());
				//notRoot();
			} 
			return foundword;
		}

		private String readMemoryDump(int[] pid) {
			String foundword = null;
			try{
				// Open the file that is the first 
				// command line parameter
				publishProgress("Reading maps\n");
				FileInputStream fstream = new FileInputStream(MAPSPATH);
				// Get the object of DataInputStream
				DataInputStream in = new DataInputStream(fstream);
				BufferedReader br = new BufferedReader(new InputStreamReader(in));

				String strLine;
				boolean mapReadable;
				//	Read File Line By Line
				int countlines = 0;
				while ((strLine = br.readLine()) != null) {
					// Print the content on the console
					countlines++;
					String[] result=strLine.split(" +");
					System.out.println (strLine + " - " + result.length);
					if(result.length==5) //no file, just own ram
					{ //analyze line
						String[] adress = result[0].split("-");
						mapReadable = (result[1].charAt(0)=='r');

						long start = Long.parseLong(adress[0], 16);
						long end = Long.parseLong(adress[1], 16);

						if(true || (end-start)>12000000)
						{
							//System.out.println("Permissions: "+result[1]+" readable: "+mapReadable+"\n");
							long sizemb = end-start;
							sizemb = sizemb/1024;
							sizemb = sizemb/1024;
							
							publishProgress("Found a possible mapping.\nDumping app memory "+adress[0]+" to "+adress[1]+" ("+sizemb+"MB)\n");
							if(!mapReadable)
								publishProgress("Mapping unreadable :(\n");
							else {
								dumpBytes(pid[PID],start,end);
								publishProgress("Trying to find words...\n");

								//if(getStringsFromMemDump())
								//{
									publishProgress("Filtering words...\n");
									foundword = filterStringFromStringsDump();
									//break;
								//} else {
									//publishProgress("Error: Could not find words.\n");
								//}
							}
						}
					}

				}
				//Close the input stream
				in.close();

				//f.delete();
				if(countlines==0)
				{
					publishProgress("Could not read maps. Please make sure Draw Something is running.\n");
				}
			}catch (Exception e){//Catch exception if any
				System.err.println("Error: " + e.getMessage());
				publishProgress("Error: " + e.getMessage());
			}
			return foundword;
		}

		private File dumpMaps(int[] pid) throws IOException {
			Process p;
			OutputStreamWriter os;
			p = Runtime.getRuntime().exec("su");
			os = new OutputStreamWriter(p.getOutputStream());
			publishProgress("Dumping maps...\n");
			os.write("cat /proc/"+pid[PID]+"/maps > "+MAPSPATH+"\n");
			//System.out.println("cat /proc/"+pid[PID]+"/maps > "+MAPSPATH+"\n");
			os.write("exit\n");
			os.flush();  
			os.close();
			try {
				p.waitFor();
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			File f = new File(MAPSPATH);
			if (!f.exists()) 
				return null;

			return f;
		}

		private int[] findPid()
		{
			String pid = null;
			int result[] = new int[2];   	

			ActivityManager am = (ActivityManager)getSystemService(Context.ACTIVITY_SERVICE);
			List<ActivityManager.RunningAppProcessInfo> l = am.getRunningAppProcesses();
			Iterator<ActivityManager.RunningAppProcessInfo> iterator = l.iterator();
			while (iterator.hasNext() && pid==null) {
				ActivityManager.RunningAppProcessInfo rapi = iterator.next();
				if(rapi.processName.equals("com.omgpop.dstfree"))
				{
					//System.out.println("PN: "+rapi.processName+" with PID: "+rapi.pid);
					pid = ""+rapi.pid;
					result[VARIANT] = DSTFREE;
					try {
						result[PID] = Integer.parseInt(pid);
					} catch(NumberFormatException nfe) {
						result[PID] = -1;
					}

					//android.os.Process.sendSignal(Integer.parseInt(pid),17);
					break;
				}
				if(rapi.processName.equals("com.omgpop.dstpaid"))
				{
					//System.out.println("PN: "+rapi.processName+" with PID: "+rapi.pid);
					pid = ""+rapi.pid;
					result[VARIANT] = DSTPAID;
					try {
						result[PID] = Integer.parseInt(pid);
					} catch(NumberFormatException nfe) {
						result[PID] = -1;
					}
					//android.os.Process.sendSignal(Integer.parseInt(pid),17);
					break;
				}
			}

			return result;
		}

		private void dumpBytes(int pid,long from,long to)
		{
			Process p;
			try {
				p = Runtime.getRuntime().exec("su");

				OutputStreamWriter os = new OutputStreamWriter(p.getOutputStream());
				//dumping
				System.out.println("to-from: "+to+"-"+from);
				String command = "dd if=/proc/"+pid+"/mem of="+DUMPPATH+" bs=4096 skip="+from/4096+" count="+((to-from)/4096);
				System.out.println(command);
				os.write(command+"\n");
				os.write("exit\n");
				os.flush();
				os.close();
				try {
					p.waitFor();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//System.out.println("Dump "+from+" done.");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		/*private boolean getStringsFromMemDump()
		{

			Process p;
			try {
				p = Runtime.getRuntime().exec("su");

				//DataInputStream is = new DataInputStream(p.getInputStream());
				OutputStreamWriter os = new OutputStreamWriter(p.getOutputStream());

				//String command = "/sbin/strings -n 3 -o "+DUMPPATH+"|grep 11663070";
				//String command = "/sbin/strings -n 3 -o "+DUMPPATH+"|grep 16037710";
				String command = "/sbin/strings -n 3 -o "+DUMPPATH+" > "+STRINGDUMPPATH;

				//System.out.println(command);
				os.write(command+"\n");
				os.write("exit\n");
				os.flush();
				try {
					p.waitFor();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				//System.out.println("getStringFromMemDump done");
				return true;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return false;

		}*/

		private String filterStringFromStringsDump()
		{
			String latestword = "";
			List<DateKey> dates = new ArrayList<DateKey>(20);
			List<Date> founddates = new ArrayList<Date>(20);
			List<Date> hidedates = new ArrayList<Date>(20);


			File f = new File(DUMPPATH);
			if (!f.exists()) 
			{
				publishProgress("Failed to open dump. Strange...\n");
				return null;
			}

			try{
				// Open the file that is the first 
				// command line parameter
				FileInputStream fstream = new FileInputStream(DUMPPATH);
				// Get the object of DataInputStream
				//DataInputStream in = new DataInputStream(fstream);
				//MyBufferedReader br = new MyBufferedReader(new InputStreamReader(in));
				//InputStreamReader isr = new InputStreamReader(in);
				String strLine;
				//	Read File Line By Line
				publishProgress("Reading...\n");
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Date date = null;
				Date latest = new Date(0);
				Pattern p = Pattern.compile("3([a-z]{3,9})([0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2})");
				
				byte charRead[] = new byte[1];
				byte readBuffer[] = new byte[35];
				byte searchableChar = 0x33; //(char)'3';

				while (fstream.read(charRead) != -1) {
					if(charRead[0]==searchableChar) {
						String x = new String(charRead);
						//System.out.println("Found 3: "+x);
						fstream.read(readBuffer);
						
						strLine = new String(readBuffer);
						
						Matcher m = p.matcher("3"+strLine);
						
						while(m.find()) {
							publishProgress("Group: "+m.group(1)+" at "+m.group(2)+"\n");
							date = null;
							try {
								date = (Date) formatter.parse(m.group(2));
							} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							if(date!=null)
							{
								if(!founddates.contains(date))
									founddates.add(date);
								else {
									if(!hidedates.contains(date))
										hidedates.add(date);
								}
								DateKey d = new DateKey();
								d.setDateTime(date);
								d.setKey(m.group(1));
								dates.add(d);
								if(date.after(latest)) {
									latest = date;
									latestword = m.group(1);
								}
							}
						}
					}

				}

				/*while ((strLine = br.readLine() ) != null)   {
					Matcher m = p.matcher(strLine);
					while(m.find()) {
						publishProgress("Group: "+m.group(1)+" at "+m.group(2)+"\n");
						date = null;
						try {
							date = (Date) formatter.parse(m.group(2));
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						if(date!=null)
						{
							if(!founddates.contains(date))
								founddates.add(date);
							else {
								if(!hidedates.contains(date))
									hidedates.add(date);
							}
							DateKey d = new DateKey();
							d.setDateTime(date);
							d.setKey(m.group(1));
							dates.add(d);
							if(date.after(latest)) {
								latest = date;
								latestword = m.group(1);
							}
						}
					}

				}*/
				Collections.sort(dates);
				Iterator<DateKey> iterator = dates.iterator();
				while (iterator.hasNext()) {
					DateKey d = iterator.next();
					if(!hidedates.contains(d.getDateTime()))
						publishProgress("Found word: "+d.getKey()+/*" at "+d.getDateTime().toLocaleString()+*/"\n");
					else
						publishProgress("-Group: "+d.getKey()+" at "+d.getDateTime().toLocaleString()+"\n");

				}


				publishProgress("Latest:"+latestword+"\n");
				/*if(latestword!=null && !latestword.equals(""))
		    			  publishProgress(latestword);
		    		  else
		    			  notFoundWord();*/
				//br.close();
				fstream.close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				System.out.println("Error occured during reading of memory dump:"+e.getMessage());
				e.printStackTrace();
			}
			return latestword;
		}

	}

}

