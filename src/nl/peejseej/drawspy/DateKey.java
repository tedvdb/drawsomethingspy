/*This file is part of DrawSomethingSpy.
 * 
 * DrawSomethingSpy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * DrawSomethingSpy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with DrawSomethingSpy.  If not, see <http://www.gnu.org/licenses/>.
 */
package nl.peejseej.drawspy;

import java.util.Date;

public class DateKey implements Comparable<DateKey> {

	  private Date dateTime;
	  private String key;

	  public Date getDateTime() {
	    return dateTime;
	  }
	  
	  public String getKey() {
		  return key;
	  }

	  public void setDateTime(Date datetime) {
	    this.dateTime = datetime;
	  }
	  
	  public void setKey(String key) {
		    this.key = key;
		  }

		public int compareTo(DateKey o) {
			// TODO Auto-generated method stub
			return getDateTime().compareTo(o.getDateTime());
		}
	}