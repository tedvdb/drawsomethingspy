/*This file is part of DrawSomethingSpy.
 * 
 * DrawSomethingSpy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * DrawSomethingSpy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with DrawSomethingSpy.  If not, see <http://www.gnu.org/licenses/>.
 */

package nl.peejseej.drawspy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;

public class MyBufferedReader extends BufferedReader {
	
	public MyBufferedReader(Reader in) {
		super(in);
	}

	public String readLine(int len) throws IOException
	{
	  int currentPos=0;
	  char[] data=new char[len];
	  int newLineChar = 10;
	  int currentChar = read();

	  System.out.println("readLine("+len+")");
	  
	  while((currentChar != newLineChar) && (currentPos<len) && currentChar!=-1)
	  {
	    data[currentPos++] = (char) currentChar;
	    currentChar=read();
	  }
	  
	  System.out.println("-readLine("+len+")");

	  if(currentChar==newLineChar)
	    return(new String(data,0,currentPos-1));
	  else
	    return(new String(data,0,currentPos));
	}
	

}
